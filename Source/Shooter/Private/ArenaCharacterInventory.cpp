#include "ArenaCharacterInventory.h"

#include "ArenaStats.h"
#include "ArenaItem.h"
#include "ArenaItemDataAsset.h"
#include "ArenaItemStatModifier.h"

#include "ArenaLogContext.h"

#pragma optimize("", off)

UArenaCharacterInventory::UArenaCharacterInventory()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UArenaCharacterInventory::InitializeComponent()
{
	Super::InitializeComponent();
	AActor* Owner = GetOwner();
	if (Owner != nullptr)
	{
		Stats = Owner->FindComponentByClass<UArenaStats>();
	}
}

void UArenaCharacterInventory::UninitializeComponent()
{
	Super::UninitializeComponent();
	Stats = nullptr;
}

void UArenaCharacterInventory::RegisterStatsComponent()
{
	AActor* Owner = GetOwner();
	Stats = Owner->FindComponentByClass<UArenaStats>();
}

void UArenaCharacterInventory::AddItem(AArenaItem* i_Item)
{
	if (Stats == nullptr)
	{
		UE_LOG(LogArena, Warning, TEXT("Missing Character Stats"));
		return;
	}

	if (i_Item == nullptr)
	{
		UE_LOG(LogArena, Warning, TEXT("Invalid Item"));
		return;
	}

	UArenaItemDataAsset* DataAsset = i_Item->ItemData;

	if (DataAsset == nullptr)
	{
		UE_LOG(LogArena, Warning, TEXT("Invalid Item DataAsset"));
		return;
	}

	UE_LOG(LogArena, Log, TEXT("Added Item %s"), *DataAsset->ItemName.ToString());

	UDataTable* Modifiers = DataAsset->StatModifiers;

	if (Modifiers == nullptr)
	{
		UE_LOG(LogArena, Warning, TEXT("Invalid Item DataTable"));
		return;
	}

	static const FString ContextString(TEXT("UArenaCharacterInventory::AddItem"));

	TArray<FArenaItemStatModifier*> Rows;
	Modifiers->GetAllRows(ContextString, Rows);

	int NumRows = Rows.Num();

	for (size_t RowIndex = 0; RowIndex < NumRows; ++RowIndex)
	{
		FArenaItemStatModifier* Row = Rows[RowIndex];
		Stats->UpdateStat(Row->StatName, Row->value, StatUpdateType::Add);
	}
}

#pragma optimize("", on)