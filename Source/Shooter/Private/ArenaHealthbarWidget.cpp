#include "ArenaHealthbarWidget.h"

void UArenaHealthbarWidget::SetMaxHealth(float NewMaxHealth, bool UpdatePercentage /* = true */)
{
	MaxHealth = FMath::Max<float>(NewMaxHealth, 0.f);
	if (MaxHealth < CurrentHealth) SetCurrentHealth(NewMaxHealth);
	if (UpdatePercentage) UpdateHealthPercentage();
}

void UArenaHealthbarWidget::SetCurrentHealth(float NewCurrentHealth, bool UpdatePercentage /* = true */)
{
	CurrentHealth = FMath::Clamp<float>(NewCurrentHealth, 0.f, MaxHealth);
	if (UpdatePercentage) UpdateHealthPercentage();
}

void UArenaHealthbarWidget::UpdateHealthPercentage()
{
	HealthPercentage = CurrentHealth / MaxHealth;
}