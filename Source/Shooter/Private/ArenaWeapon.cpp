// Fill out your copyright notice in the Description page of Project Settings.

#include "ArenaWeapon.h"

#include "ArenaCharacter.h"
#include "ArenaBullet.h"
#include "ArenaWeaponStats.h"

AArenaWeapon::AArenaWeapon()
{
	PrimaryActorTick.bCanEverTick = true;
	MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Muzzle = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	StatsComponent = CreateDefaultSubobject<UArenaWeaponStats>(TEXT("Stats"));
}

void AArenaWeapon::BeginPlay()
{
	Super::BeginPlay();
}

void AArenaWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AArenaWeapon::Fire()
{
	FActorSpawnParameters params;
	params.Instigator = Cast<APawn>(this);
	params.Owner = this;
	params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	FVector bulletSpawnLocation;
	FRotator bulletSpawnRotation;
	CalculateBulletSpawnData(bulletSpawnLocation, bulletSpawnRotation);

	AArenaBullet* Spawned = GetWorld()->SpawnActor<AArenaBullet>(Bullet.Get(), bulletSpawnLocation, bulletSpawnRotation, params);

	AActor* OwnerActor = GetOwner();
	AArenaCharacter* OwnerCharacter = Cast<AArenaCharacter>(GetOwner());

	float BaseDamage = StatsComponent->GetFloatStat("BaseDamage");

	float TotalDamage = OwnerCharacter != nullptr
		? OwnerCharacter->GetTotalDamage(BaseDamage)
		: BaseDamage;

	float InitialSpeed = StatsComponent->GetFloatStat("InitialSpeed");
	float MaxSpeed = StatsComponent->GetFloatStat("MaxSpeed");
	float Range = StatsComponent->GetFloatStat("Range");

	Spawned->SetupBulletData(TotalDamage, InitialSpeed, MaxSpeed, Range);
}

void AArenaWeapon::CalculateBulletSpawnData(FVector& SpawnPosition, FRotator& SpawnRotation)
{
	SpawnPosition = GetActorLocation() + Muzzle->GetComponentLocation();
	SpawnRotation = GetActorRotation() + Muzzle->GetComponentRotation();

	SpawnRotation.Roll = 0.f;
	SpawnRotation.Pitch = 0.f;
	SpawnRotation.Yaw += 90.f;
}

float AArenaWeapon::GetFireRate()
{
	return StatsComponent->GetFloatStat("FireRate", 1.f);
}