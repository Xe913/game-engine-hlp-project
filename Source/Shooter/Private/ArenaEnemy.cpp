#include "ArenaEnemy.h"
#include "ArenaEnemyStats.h"
#include "ArenaItem.h"

AArenaEnemy::AArenaEnemy() : Super()
{
	StatsComponent = CreateDefaultSubobject<UArenaEnemyStats>(TEXT("Stats"));
}

void AArenaEnemy::DestroyCharacter()
{
	Super::DestroyCharacter();
	SpawnDrop();
}

void AArenaEnemy::SpawnDrop()
{
	float DropRate = StatsComponent->GetFloatStat("DropRate");
	float Value = FMath::FRandRange(0, 1);
	if (Value <= DropRate)
	{
		int DropIndex = FMath::RandRange(0, Drops.Num() - 1);

		FActorSpawnParameters params;
		params.Instigator = Cast<APawn>(this);
		params.Owner = this;
		params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		FVector dropSpawnLocation;
		FRotator dropSpawnRotation;

		AArenaItem* Spawned = GetWorld()->SpawnActor<AArenaItem>(Drops[DropIndex].Get(), GetActorLocation(), FRotator::ZeroRotator, params);
	}
}