#include "ArenaCharacter.h"

#include "ArenaWeapon.h"
#include "ArenaCharacterStats.h"
#include "ArenaCharacterInventory.h"
#include "ArenaItem.h"
#include "ArenaHealthbarWidget.h"
#include "Components/WidgetComponent.h"

#pragma optimize("", off)

AArenaCharacter::AArenaCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	HealthBarWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("Health Bar"));
	HealthBarWidgetComponent->SetupAttachment(GetRootComponent());
 	HealthBarWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
 	HealthBarWidgetComponent->SetDrawSize(FVector2D(100, 15));
 	HealthBarWidgetComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 100.0f));
 	HealthBarWidgetComponent->SetRelativeRotation(FQuat::Identity);

}

void AArenaCharacter::BeginPlay()
{
	Super::BeginPlay();
	if (Weapon != nullptr)
	{
		EquippedWeapon = GetWorld()->SpawnActor<AArenaWeapon>(Weapon, GetTransform());
		const FAttachmentTransformRules AttachRules(EAttachmentRule::SnapToTarget, false);
		EquippedWeapon->AttachToComponent(GetMesh(), AttachRules, WeaponSocket);
		EquippedWeapon->SetOwner(this);
	}
	if (HealthBarWidgetComponent != nullptr)
	{
		SetHealthbarVisibility(DisplayHealthbar);
	}
}

void AArenaCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AArenaCharacter::MoveUp(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AArenaCharacter::MoveRight(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void AArenaCharacter::StartFiring()
{
	if (ContinuousFire)
	{
		float FireInterval = CalculateFireInterval();
		if (FireInterval > 0)
		{
			GetWorld()->GetTimerManager().SetTimer(
				KeepFiringHandle, this, &AArenaCharacter::Fire,
				FireInterval, true, 0.f);
		}
		Firing = true;
	}
	else
	{
		Fire();
	}
}

void AArenaCharacter::StopFiring()
{
	if (ContinuousFire && GetWorld()->GetTimerManager().IsTimerActive(KeepFiringHandle))
	{
		GetWorld()->GetTimerManager().ClearTimer(KeepFiringHandle);
		Firing = false;
	}
}

void AArenaCharacter::Fire()
{
	EquippedWeapon->Fire();
}


float AArenaCharacter::CalculateFireInterval()
{
	float NewRate = EquippedWeapon->GetFireRate() * StatsComponent->GetFloatStat("FireRateMultiplier");
	if (NewRate == 0) return -1;
	return 1.f / NewRate;
}

void AArenaCharacter::UpdateFireRate(float NewFireRate)
{
	if (ContinuousFire)
	{
		GetWorld()->GetTimerManager().ClearTimer(KeepFiringHandle);
		if (Firing)
		{
			float FireInterval = CalculateFireInterval();
			if (FireInterval > 0)
			{
				GetWorld()->GetTimerManager().SetTimer(
					KeepFiringHandle, this, &AArenaCharacter::Fire,
					FireInterval, true, 0.f);
			}
		}
	}
}

float AArenaCharacter::GetTotalDamage(float BaseDamage)
{
	float TotalDamage = BaseDamage;
	for (FString Stat : AddToBaseDamageStats)
	{
		TotalDamage += StatsComponent->GetFloatStat(Stat);
	}
	for (FString Stat : MultiplierStats)
	{
		TotalDamage *= StatsComponent->GetFloatStat(Stat, 1.f);
	}
	for (FString Stat : AddToMultipliedDamageStats)
	{
		TotalDamage += StatsComponent->GetFloatStat(Stat);
	}
	return TotalDamage;
}

void AArenaCharacter::ProcessDamage(float Damage)
{
	int TotalDamage = Damage * StatsComponent->GetFloatStat("DamageReduction", 1.f); //I use a fake stat on purpose to demonstrate the custom fallback case
	StatsComponent->UpdateStat("CurrentHealth", -Damage, StatUpdateType::Add);
	//if (DisplayHealthbar) UpdateHealthbar(); //changed logic to have the stat component update the healthbar so it works on healing too. Still not sure it's the right way to do it
	if (StatsComponent->GetFloatStat("CurrentHealth") <= 0) DestroyCharacter();
}

void AArenaCharacter::NotifyActorBeginOverlap(AActor* OtherActor) {
	Super::NotifyActorBeginOverlap(OtherActor);
	if (OtherActor == nullptr || OtherActor->IsPendingKill())
	{
		return;
	}
	if (InventoryComponent != nullptr)
	{
		bool bIsItem = OtherActor->IsA<AArenaItem>();
		if (bIsItem)
		{
			AArenaItem* Item = Cast<AArenaItem>(OtherActor);
			InventoryComponent->AddItem(Item);
			Item->Destroy();
		}
	}
}

void AArenaCharacter::DestroyCharacter()
{
	OnKill.ExecuteIfBound(this);
	if (EquippedWeapon != nullptr) EquippedWeapon->Destroy();
	this->Destroy();
}

void AArenaCharacter::UpdateHealthbar()
{
 	if (HealthBarWidgetComponent != nullptr && HealthBarWidgetComponent->GetUserWidgetObject() != nullptr)
 	{
		float MaxHealth = StatsComponent->GetFloatStat("TotalHealth");
		float CurrentHealth = StatsComponent->GetFloatStat("CurrentHealth");
 		(Cast<UArenaHealthbarWidget>(HealthBarWidgetComponent->GetUserWidgetObject()))->SetMaxHealth(MaxHealth, false);
 		(Cast<UArenaHealthbarWidget>(HealthBarWidgetComponent->GetUserWidgetObject()))->SetCurrentHealth(CurrentHealth);
 	}
}

void AArenaCharacter::SetHealthbarVisibility(bool MakeVisible)
{
	if (MakeVisible) UpdateHealthbar();
	HealthBarWidgetComponent->SetVisibility(MakeVisible);
}

bool AArenaCharacter::IsFiring()
{
	return Firing;
}

#pragma optimize("", on)