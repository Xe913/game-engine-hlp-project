// Fill out your copyright notice in the Description page of Project Settings.

#include "ArenaCharacterAim.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/KismetMathLibrary.h"

void UArenaCharacterAim::RotateTowardPointer(OUT FRotator& OutRotation)
{
	FVector CharacterPosition = GetActorLocation();
	APlayerController* Controller = GetWorld()->GetFirstPlayerController();
	FVector PointerPosition = FVector::ZeroVector;
	FVector PointerRotation = FVector::ZeroVector;
	Controller->DeprojectMousePositionToWorld(PointerPosition, PointerRotation);
	FVector Target = FMath::RayPlaneIntersection(PointerPosition, PointerRotation, FPlane(CharacterPosition, FVector::UpVector));
	OutRotation = UKismetMathLibrary::FindLookAtRotation(CharacterPosition, Target);
}