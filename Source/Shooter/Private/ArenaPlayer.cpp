#include "ArenaPlayer.h"

#include "ArenaCharacterStats.h"
#include "ArenaCharacterInventory.h"
#include "ArenaCharacterAim.h"
#include "Kismet/GameplayStatics.h"

AArenaPlayer::AArenaPlayer()
	: Super()
{
	StatsComponent = CreateDefaultSubobject<UArenaCharacterStats>(TEXT("Stats"));
	InventoryComponent = CreateDefaultSubobject<UArenaCharacterInventory>(TEXT("Inventory"));
	InventoryComponent->RegisterStatsComponent();
	AimComponent = CreateDefaultSubobject<UArenaCharacterAim>(TEXT("Aim Component"));
}

void AArenaPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FRotator NewRotation;
	AimComponent->RotateTowardPointer(NewRotation);
	SetActorRotation(NewRotation);
}

void AArenaPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveUp", this, &AArenaPlayer::MoveUp);
	PlayerInputComponent->BindAxis("MoveRight", this, &AArenaPlayer::MoveRight);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AArenaPlayer::StartFiring);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AArenaPlayer::StopFiring);
}

void AArenaPlayer::DestroyCharacter()
{
	Super::DestroyCharacter();
	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
}