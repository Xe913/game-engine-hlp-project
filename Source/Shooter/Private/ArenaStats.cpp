#include "ArenaStats.h"
#include "ArenaCharacter.h"
#include "ReflectionUtilsFunctionLibrary.h"
#include "Math/NumericLimits.h"

UArenaStats::UArenaStats()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UArenaStats::UpdateStat(const FString& StatName, float StatValue, StatUpdateType UpdateType /* StatUpdateType::Override */)
{
	FString Path = "This";
	Path += ".";

	FString MinPath = Path;
	MinPath += MinPrefix;

	FString MaxPath = Path;
	MaxPath += MaxPrefix;

	Path += StatName;
	MinPath += StatName;
	MaxPath += StatName;

	void* OutObject = nullptr;

	FFloatProperty* FloatProperty = nullptr;
	FFloatProperty* MinFloatProperty = nullptr;
	FFloatProperty* MaxFloatProperty = nullptr;
	FindFloatProperty(OutObject, FloatProperty, Path);
	FindFloatProperty(OutObject, MinFloatProperty, MinPath);
	FindFloatProperty(OutObject, MaxFloatProperty, MaxPath);

	if (FloatProperty != nullptr)
	{
		switch (UpdateType)
		{
		case StatUpdateType::Override:
			break;
		case StatUpdateType::Add:
			StatValue += FloatProperty->GetPropertyValue_InContainer(OutObject);
			break;
		case StatUpdateType::Multiply:
			StatValue *= FloatProperty->GetPropertyValue_InContainer(OutObject);
			break;
		}

		float MinValue = (MinFloatProperty != nullptr)
			? MinFloatProperty->GetPropertyValue_InContainer(OutObject)
			: TNumericLimits<float>::Min();
		float MaxValue = (MaxFloatProperty != nullptr)
			? MaxFloatProperty->GetPropertyValue_InContainer(OutObject)
			: TNumericLimits<float>::Max();

		float Value = FMath::Clamp(StatValue, MinValue, MaxValue);

		Value = ExecutePreUpdate(Value, StatName);

		Value = FMath::Clamp(Value, MinValue, MaxValue);

		FloatProperty->SetPropertyValue_InContainer(OutObject, Value);

		ExecutePostUpdate(Value, StatName);
	}
}

float UArenaStats::ExecutePreUpdate(float NewValue, const FString& StatName)
{
	return NewValue;
}

float UArenaStats::ExecutePostUpdate(float NewValue, const FString& StatName)
{
	return NewValue;
}

void UArenaStats::FindFloatProperty(void*& OutObject, FFloatProperty*& OutProperty, const FString& PropertyPath)
{
	FProperty* StatProperty = UReflectionUtilsFunctionLibrary::RetrieveProperty(this, PropertyPath, OutObject);
	if (StatProperty != nullptr)
	{
		OutProperty = CastField<FFloatProperty>(StatProperty);
	}
}

float UArenaStats::GetFloatStat(const FString& StatName, float DefaultValue /* = 0 */)
{
	FString Path = "This";
	Path += ".";
	Path += StatName;

	void* OutObject = nullptr;
	FFloatProperty* FloatProperty = nullptr;
	FindFloatProperty(OutObject, FloatProperty, Path);

	if (FloatProperty != nullptr)
	{
		return FloatProperty->GetPropertyValue_InContainer(OutObject);
	}
	return DefaultValue;
}