#include "ArenaBullet.h"
#include "ArenaCharacter.h"

AArenaBullet::AArenaBullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	RootComponent = MeshComponent;

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Movement Component"));
	MovementComponent->UpdatedComponent = MeshComponent;

	MovementComponent->InitialSpeed = 3000.f;
	MovementComponent->MaxSpeed = 4000.f;
	MovementComponent->ProjectileGravityScale = 0.f;
}

// Called when the game starts or when spawned
void AArenaBullet::BeginPlay()
{
	Super::BeginPlay();
	if (MeshComponent)
	{
		MeshComponent->IgnoreActorWhenMoving(GetInstigator(), true);
		MeshComponent->OnComponentHit.AddDynamic(this, &AArenaBullet::OnHit);
	}
}

// Called every frame
void AArenaBullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AArenaBullet::SetupBulletData(float Damage, float InitialSpeed, float MaxSpeed, float Range)
{
	BulletDamage = FMath::Max3(Damage, 0.f, 0.f);
	MovementComponent->InitialSpeed = InitialSpeed;
	MovementComponent->MaxSpeed = MaxSpeed;
	float lifespan =  2 * Range / (MovementComponent->InitialSpeed + MovementComponent->MaxSpeed);
	SetLifeSpan(lifespan);
}

void AArenaBullet::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	AArenaCharacter* HitCharacter = Cast<AArenaCharacter>(OtherActor);
	if (HitCharacter)
	{
		HitCharacter->ProcessDamage(BulletDamage);
	}
	Destroy();
}