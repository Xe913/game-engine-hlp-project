#include "ArenaSpawner.h"
#include "ArenaCharacter.h"

AArenaSpawner::AArenaSpawner()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AArenaSpawner::BeginPlay()
{
	Super::BeginPlay();
}

void AArenaSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

AArenaCharacter* AArenaSpawner::SpawnEnemy(bool ForceActivation /* = false */)
{
	if (!ForceActivation && !CanSpawnEnemies()) return nullptr;

	FActorSpawnParameters params;
	params.Instigator = Cast<APawn>(this);
	AArenaCharacter* SpawnedEnemy = GetWorld()->SpawnActor<AArenaCharacter>(SpawnableEnemy, GetActorLocation(), GetActorRotation(), params);
	if (SpawnedEnemy)
	{
		SpawnedEnemy->OnKill.BindUFunction(this, FName("SpawnedEnemyKilled"));
		CurrentlySpawnedEnemies++;
		AlreadySpawnedEnemies++;
	}
	return SpawnedEnemy;
}

void AArenaSpawner::SpawnedEnemyKilled(AArenaCharacter* KilledEnemy)
{
	CurrentlySpawnedEnemies--;
}

void AArenaSpawner::RegisterToSpawnerManager(UArenaSpawnerManager* SpawnerManager)
{
	Manager = SpawnerManager;
}

void AArenaSpawner::RemoveFromSpawnerManager()
{
	Manager = nullptr;
}

UArenaSpawnerManager* AArenaSpawner::GetSpawnerManager()
{
	return Manager;
}

int AArenaSpawner::GetCurrentlySpawnedEnemies()
{
	return CurrentlySpawnedEnemies;
}

int AArenaSpawner::GetMaxSpawnedEnemies()
{
	return MaxSpawnedEnemies;
}

int AArenaSpawner::GetContainedEnemies()
{
	return ContainedEnemies;
}

int AArenaSpawner::GetAlreadySpawnedEnemies()
{
	return AlreadySpawnedEnemies;
}

bool AArenaSpawner::CanSpawnEnemies()
{
	if (MaxSpawnedEnemies < 0) return true;
	if (ContainedEnemies < 0) return true;
	
	return  MaxSpawnedEnemies - CurrentlySpawnedEnemies > 0 && GetEnemiesLeft() > 0;
}

int AArenaSpawner::GetEnemiesLeft()
{
	return ContainedEnemies >= 0
		? ContainedEnemies - AlreadySpawnedEnemies
		: -1;
}

bool AArenaSpawner::IsStartedByMainSpawnerManager()
{
	return StartedByMainSpawnerManager;
}