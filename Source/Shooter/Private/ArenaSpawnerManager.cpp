#include "ArenaSpawnerManager.h"
#include "ArenaSpawner.h"
#include "EngineUtils.h"
#include "Kismet/GameplayStatics.h"

UArenaSpawnerManager::UArenaSpawnerManager()
{
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UArenaSpawnerManager::BeginPlay()
{
	Super::BeginPlay();
	StartSpawners();
}

void UArenaSpawnerManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UArenaSpawnerManager::StartSpawners()
{
	//UGameplayStatics::GetAllActorsOfClass(GetWorld(), AArenaSpawner::StaticClass(), Spawners);

	//personalized version of GetAllActorsOfClass, used to directly return ArenaSpawners instead of Actors
	if (UWorld* World = GEngine->GetWorldFromContextObject(GetWorld(), EGetWorldErrorMode::LogAndReturnNull))
	{
		for (TActorIterator<AArenaSpawner> It(World, AArenaSpawner::StaticClass()); It; ++It)
		{
			AArenaSpawner* Spawner = *It;
			if (Spawner->IsStartedByMainSpawnerManager())
			{
				RegisterSpawner(Spawner);
			}
		}
	}

	SpawnTimerDelegate.BindUFunction(this, FName("ActivateRandomSpawner"));
	GetWorld()->GetTimerManager().SetTimer(SpawnTimerHandle, SpawnTimerDelegate, SpawnEverySeconds, true, 0.f);
}

int UArenaSpawnerManager::RegisterSpawner(AArenaSpawner* Spawner)
{
	int index = Spawners.AddUnique(Spawner);
	if (index >= 0 && index < Spawners.Num())
	{
		Spawner->RegisterToSpawnerManager(this);
		if (Spawner->CanSpawnEnemies())
		{
			AvailableSpawners.AddUnique(Spawner);
		}
		MaxSpawnedEnemies += Spawner->GetMaxSpawnedEnemies();
		return index;
	}
	return -1; //return -1 if the spawner wasn't registered
}

int UArenaSpawnerManager::RemoveSpawner(AArenaSpawner* Spawner)
{
	int removed = Spawners.Remove(Spawner);
	if (removed > 0)
	{
		Spawner->RemoveFromSpawnerManager();
		AvailableSpawners.Remove(Spawner);
		MaxSpawnedEnemies -= Spawner->GetMaxSpawnedEnemies();
		return removed;
	}
	return -1; //to maintain the parallel between register and remove, -1 means no spawner was removed
}

AArenaSpawner* UArenaSpawnerManager::GetRandomSpawner(bool IncludeFullSpawners /* = false */)
{
	TArray<AArenaSpawner*>& Target = IncludeFullSpawners ? Spawners : AvailableSpawners;
	if (Target.Num() > 0)
	{
		int index = FMath::RandRange(0, Target.Num() - 1);
		return Target[index];
	}
	return nullptr;
}

void UArenaSpawnerManager::ActivateRandomSpawner()
{
	ActivateSpawner();
}

AArenaCharacter* UArenaSpawnerManager::ActivateSpawner(AArenaSpawner* Spawner, bool ForceActivation)
{
	if (!ForceActivation && !CanSpawnEnemies()) return nullptr;

	if (!Spawner) Spawner = GetRandomSpawner(ForceActivation);
	if (Spawner)
	{
		AArenaCharacter* SpawnedEnemy = Spawner->SpawnEnemy(ForceActivation);
		if (!Spawner->CanSpawnEnemies())
		{
			AvailableSpawners.Remove(Spawner);
		}
		return SpawnedEnemy;
	}
	return nullptr;
}

AArenaSpawner* GetRandomSpawner(bool IncludeFullSpawners /* = false */)
{
	return nullptr;
}

int UArenaSpawnerManager::GetNumberOfSpawners()
{
	return Spawners.Num();
}

int UArenaSpawnerManager::GetMaxSpawnedEnemies()
{
	return MaxSpawnedEnemies;
}

int UArenaSpawnerManager::GetEnemiesToKill()
{
	return EnemiesToKill;
}

int UArenaSpawnerManager::GetTotalSpawnedEnemies()
{
	return TotalSpawnedEnemies;
}

int UArenaSpawnerManager::GetKilledEnemies()
{
	return KilledEnemies;
}

int UArenaSpawnerManager::GetCurrentlySpawnedEnemies()
{
	return TotalSpawnedEnemies - KilledEnemies;
}

bool UArenaSpawnerManager::CanSpawnEnemies()
{
	return MaxSpawnedEnemies - GetCurrentlySpawnedEnemies() > 0;
}

int UArenaSpawnerManager::GetEnemiesLeft()
{
	return EnemiesToKill - KilledEnemies;
}