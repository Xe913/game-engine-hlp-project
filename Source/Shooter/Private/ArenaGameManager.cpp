#include "ArenaGameManager.h"
#include "ArenaSpawnerManager.h"

AArenaGameManager::AArenaGameManager()
{
	PrimaryActorTick.bCanEverTick = true;
	SpawnerManager = CreateDefaultSubobject<UArenaSpawnerManager>(TEXT("SpawnerManager"));
}

void AArenaGameManager::BeginPlay()
{
	Super::BeginPlay();
}

void AArenaGameManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

UArenaSpawnerManager* AArenaGameManager::GetSpawnerManager()
{
	return SpawnerManager;
}