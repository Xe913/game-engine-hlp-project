#include "ArenaAIController.h"
#include "ArenaEnemy.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

void AArenaAIController::BeginPlay()
{
	Super::BeginPlay();
	APawn* ControlledPawn = GetPawn();
	ControlledEnemy = Cast<AArenaEnemy>(ControlledPawn);
	ACharacter* PlayerCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	Player = Cast<AArenaCharacter>(PlayerCharacter);

	PrimaryActorTick.bCanEverTick = PlayerCharacter != nullptr;
}

void AArenaAIController::Tick(float DeltaTime)
{
	if (Player != nullptr)
	{
		MoveToActor(Player);
		
		if (ControlledEnemy == nullptr)
		{
			APawn* ControlledPawn = GetPawn();
			ControlledEnemy = Cast<AArenaEnemy>(ControlledPawn);
		}
		if (ControlledEnemy != nullptr)
		{
			FVector EnemyLocation = ControlledEnemy->GetActorLocation();
			FVector PlayerLocation = Player->GetActorLocation();
			FRotator ControlledEnemyRotation = UKismetMathLibrary::FindLookAtRotation(EnemyLocation, PlayerLocation);
			ControlledEnemy->SetActorRotation(ControlledEnemyRotation);
			if (!ControlledEnemy->IsFiring() && FVector::Dist(EnemyLocation, PlayerLocation) <= AttackRange)
			{
				ControlledEnemy->StartFiring();
			}
			else if (ControlledEnemy->IsFiring() && FVector::Dist(EnemyLocation, PlayerLocation) > AttackRange)
			{
				ControlledEnemy->StopFiring();
			}
		}
	}
}