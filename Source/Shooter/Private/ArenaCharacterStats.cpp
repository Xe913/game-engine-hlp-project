#include "ArenaCharacterStats.h"
#include "ArenaCharacter.h"

float UArenaCharacterStats::ExecutePostUpdate(float NewValue, const FString& StatName)
{
	AArenaCharacter* Owner = Cast<AArenaCharacter>(GetOwner());

	if (StatName == "FireRateMultiplier")
	{
		if (Owner != nullptr)
		{
			Owner->UpdateFireRate(NewValue);
			return NewValue;
		}
	}

	if (StatName == "CurrentHealth")
	{
		if (NewValue > TotalHealth) CurrentHealth = TotalHealth;
		Owner->UpdateHealthbar();
		return CurrentHealth;
	}

	if (StatName == "TotalHealth")
	{
		if (NewValue < CurrentHealth) TotalHealth = CurrentHealth;
		Owner->UpdateHealthbar();
		return TotalHealth;
	}

	return 0.f;
}