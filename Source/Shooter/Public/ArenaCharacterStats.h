#pragma once

#include "CoreMinimal.h"
#include "ArenaStats.h"
#include "ArenaCharacterStats.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER_API UArenaCharacterStats : public UArenaStats
{
	GENERATED_BODY()

protected:
	virtual float ExecutePostUpdate(float NewValue, const FString& StatName) override;

	//stats
	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0.0", ClampMax = "999.0", UIMin = "0.0", UIMax = "999.0"))
	float TotalHealth = 100.f;
	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0.0", ClampMax = "999.0", UIMin = "0.0", UIMax = "999.0"))
	float CurrentHealth = 100.f;
	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float DamageMultiplier = 1.f;
	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0.0001", ClampMax = "1.0", UIMin = "0.0001", UIMax = "1.0"))
	float FireRateMultiplier = 1.f;

	//min and max values
	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "999.0", UIMin = "0.0", UIMax = "999.0"))
	float MinTotalHealth = 1.f;
	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "999.0", UIMin = "0.0", UIMax = "999.0"))
	float MaxTotalHealth = 999.f;

	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "999.0", UIMin = "0.0", UIMax = "999.0"))
	float MinCurrentHealth = 0.f;
	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "999.0", UIMin = "0.0", UIMax = "999.0"))
	float MaxCurrentHealth = 999.f;

	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "999.0", UIMin = "0.0", UIMax = "999.0"))
	float MinDamageMultiplier = 0.1f;
	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "999.0", UIMin = "0.0", UIMax = "999.0"))
	float MaxDamageMultiplier = 50.f;

	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0001", ClampMax = "999.0", UIMin = "0.0", UIMax = "999.0"))
	float MinFireRateMultiplier = 0.1f;
	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0001", ClampMax = "999.0", UIMin = "0.0", UIMax = "999.0"))
	float MaxFireRateMultiplier = 20.f;
};
