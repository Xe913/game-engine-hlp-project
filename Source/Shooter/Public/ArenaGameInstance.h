// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "ArenaGameInstance.generated.h"

class UArenaSpawnerManager;

UCLASS()
class SHOOTER_API UArenaGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	virtual void Init() override;
};
