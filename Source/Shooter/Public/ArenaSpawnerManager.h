#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ArenaSpawnerManager.generated.h"

class AArenaSpawner;
class AArenaCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER_API UArenaSpawnerManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	UArenaSpawnerManager();

	virtual void BeginPlay() override;

protected:
	void StartSpawners();
	AArenaSpawner* GetRandomSpawner(bool IncludeFullSpawners = false);

	UPROPERTY(EditDefaultsOnly, Category = "Spawner Manager")
	int EnemiesToKill;
	UPROPERTY(EditDefaultsOnly, Category = "Spawner Manager")
	int MaxSpawnedEnemies;
	UPROPERTY(EditDefaultsOnly, Category = "Spawner Manager")
	float SpawnEverySeconds;

	TArray<AArenaSpawner*> Spawners;
	TArray<AArenaSpawner*> AvailableSpawners;
	int TotalSpawnedEnemies;
	int KilledEnemies;

	int WaitForSpawners;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//centralized management
	UFUNCTION(BlueprintCallable)
	int RegisterSpawner(AArenaSpawner* Spawner);
	UFUNCTION(BlueprintCallable)
	int RemoveSpawner(AArenaSpawner* Spawner);
	UFUNCTION(BlueprintCallable)
	int GetNumberOfSpawners();

	UFUNCTION(BlueprintCallable)
	void ActivateRandomSpawner();
	UFUNCTION(BlueprintCallable)
	AArenaCharacter* ActivateSpawner(AArenaSpawner* Spawner = nullptr, bool ForceActivation = false);

	UFUNCTION(BlueprintCallable)
	int GetEnemiesToKill();
	UFUNCTION(BlueprintCallable)
	int GetMaxSpawnedEnemies();

	UFUNCTION(BlueprintCallable)
	int GetTotalSpawnedEnemies();
	UFUNCTION(BlueprintCallable)
	int GetKilledEnemies();

	UFUNCTION(BlueprintCallable)
	int GetCurrentlySpawnedEnemies();
	UFUNCTION(BlueprintCallable)
	bool CanSpawnEnemies();
	UFUNCTION(BlueprintCallable)
	int GetEnemiesLeft();

	FTimerHandle SpawnTimerHandle;
	FTimerDelegate SpawnTimerDelegate;
};
