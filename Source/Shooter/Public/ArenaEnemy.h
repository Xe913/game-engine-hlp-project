#pragma once

#include "CoreMinimal.h"
#include "ArenaCharacter.h"
#include "ArenaEnemy.generated.h"

class AArenaItem;

UCLASS()
class SHOOTER_API AArenaEnemy : public AArenaCharacter
{
	GENERATED_BODY()

public:
	AArenaEnemy();

protected:
	virtual void DestroyCharacter() override;
	virtual void SpawnDrop();

	UPROPERTY(EditAnywhere, Category = "Drops")
	TArray<TSubclassOf<AArenaItem>> Drops;
};
