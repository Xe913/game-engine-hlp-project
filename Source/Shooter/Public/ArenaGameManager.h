#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ArenaGameManager.generated.h"

class UArenaSpawnerManager;

UCLASS()
class SHOOTER_API AArenaGameManager : public AActor
{
	GENERATED_BODY()
	
public:	
	AArenaGameManager();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	UArenaSpawnerManager* SpawnerManager;

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	UArenaSpawnerManager* GetSpawnerManager();
};
