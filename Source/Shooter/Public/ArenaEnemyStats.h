#pragma once

#include "CoreMinimal.h"
#include "ArenaCharacterStats.h"
#include "ArenaEnemyStats.generated.h"

UCLASS()
class SHOOTER_API UArenaEnemyStats : public UArenaCharacterStats
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float DropRate = 0.2f;
};
