// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "ArenaBullet.generated.h"

UCLASS()
class SHOOTER_API AArenaBullet : public AActor
{
	GENERATED_BODY()
	
public:
	AArenaBullet();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
	class UStaticMeshComponent* MeshComponent;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
	class UProjectileMovementComponent* MovementComponent;

	UPROPERTY(BlueprintReadOnly)
	float BulletDamage = 0.f;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void SetupBulletData(float Damage, float InitialSpeed, float MaxSpeed, float Range);
};