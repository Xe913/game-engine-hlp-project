// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ArenaStats.h"
#include "ArenaWeaponStats.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_API UArenaWeaponStats : public UArenaStats
{
	GENERATED_BODY()

protected:
	//stats
	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0.0", ClampMax = "999.0", UIMin = "0.0", UIMax = "999.0"))
	float BaseDamage = 1.f;
	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0.0", ClampMax = "99999.0", UIMin = "0.0", UIMax = "99999.0"))
	float InitialSpeed = 1000.f;
	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0.0", ClampMax = "99999.0", UIMin = "0.0", UIMax = "99999.0"))
	float MaxSpeed = 1000.f;
	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0.0", ClampMax = "99999.0", UIMin = "0.0", UIMax = "99999.0"))
	float Range = 500.f;
	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0.0001", ClampMax = "999.0", UIMin = "0.0001", UIMax = "999.0"))
	float FireRate = 0.3f;

	//min and max values
	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "999.0", UIMin = "0.0", UIMax = "999.0"))
		float MinBaseDamage = 0.f;
	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "999.0", UIMin = "0.0", UIMax = "999.0"))
		float MaxBaseDamage = 999.f;

	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "99999.0", UIMin = "0.0", UIMax = "99999.0"))
		float MinInitialSpeed = 0.f;
	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "99999.0", UIMin = "0.0", UIMax = "99999.0"))
		float MaxInitialSpeed = 99999.f;

	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "99999.0", UIMin = "0.0", UIMax = "99999.0"))
		float MinMaxSpeed = 0.f;
	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "99999.0", UIMin = "0.0", UIMax = "99999.0"))
		float MaxMaxSpeed = 99999.f;

	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "99999.0", UIMin = "0.0", UIMax = "99999.0"))
		float MinRange = 0.f;
	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "99999.0", UIMin = "0.0", UIMax = "99999.0"))
		float MaxRange = 99999.f;

	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0", ClampMax = "999.0", UIMin = "0.0", UIMax = "999.0"))
		float MinFireRate = 0.f;
	UPROPERTY(EditAnywhere, Category = "Stats|Min & Max Values", meta = (ClampMin = "0.0001", ClampMax = "999.0", UIMin = "0.0001", UIMax = "999.0"))
		float MaxFireRate = 999.f;
};
