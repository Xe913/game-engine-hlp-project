// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "ArenaCharacterAim.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER_API UArenaCharacterAim : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	void RotateTowardPointer(OUT FRotator& OutRotation);
};
