#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ArenaSpawner.generated.h"

class AArenaCharacter;
class UArenaSpawnerManager;

UCLASS()
class SHOOTER_API AArenaSpawner : public AActor
{
	GENERATED_BODY()
	
public:
	AArenaSpawner();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Spawner")
	TSubclassOf<AArenaCharacter> SpawnableEnemy;
	UPROPERTY(EditAnywhere, Category = "Spawner")
	int MaxSpawnedEnemies = -1;
	UPROPERTY(EditAnywhere, Category = "Spawner")
	int ContainedEnemies = -1;

	int CurrentlySpawnedEnemies;
	int AlreadySpawnedEnemies;

	UArenaSpawnerManager* Manager;
	bool StartedByMainSpawnerManager = true;

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	AArenaCharacter* SpawnEnemy(bool ForceActivation = false);
	UFUNCTION(BlueprintCallable)
	void SpawnedEnemyKilled(AArenaCharacter* KilledEnemy);

	//centralized management
	UFUNCTION(BlueprintCallable)
	void RegisterToSpawnerManager(UArenaSpawnerManager* SpawnerManager);
	UFUNCTION(BlueprintCallable)
	void RemoveFromSpawnerManager();
	UFUNCTION(BlueprintCallable)
	UArenaSpawnerManager* GetSpawnerManager();
	UFUNCTION(BlueprintCallable)
	bool IsStartedByMainSpawnerManager();

	//basic getters
	UFUNCTION(BlueprintCallable)
	int GetCurrentlySpawnedEnemies();
	UFUNCTION(BlueprintCallable)
	int GetMaxSpawnedEnemies();
	UFUNCTION(BlueprintCallable)
	int GetContainedEnemies();
	UFUNCTION(BlueprintCallable)
	int GetAlreadySpawnedEnemies();

	//utility getters (return values calculated from other attributes)
	UFUNCTION(BlueprintCallable)
	bool CanSpawnEnemies();
	UFUNCTION(BlueprintCallable)
	int GetEnemiesLeft();
};
