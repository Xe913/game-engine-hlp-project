#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "ArenaAIController.generated.h"

class AArenaEnemy;
class AArenaCharacter;

UCLASS()
class SHOOTER_API AArenaAIController : public AAIController
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly, Category = "AI")
	float AttackRange = 1000.f;

	AArenaEnemy* ControlledEnemy;
	AArenaCharacter* Player;
};
