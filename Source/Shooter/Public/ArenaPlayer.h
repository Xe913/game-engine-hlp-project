#pragma once

#include "CoreMinimal.h"
#include "ArenaCharacter.h"
#include "ArenaPlayer.generated.h"

class UArenaCharacterAim;

UCLASS()
class SHOOTER_API AArenaPlayer : public AArenaCharacter
{
	GENERATED_BODY()

public:
	AArenaPlayer();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
	UArenaCharacterAim* AimComponent;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void DestroyCharacter() override;

public:
	virtual void Tick(float DeltaTime) override;
};
