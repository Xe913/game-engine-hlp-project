// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "ArenaBullet.h"
#include "ArenaWeapon.generated.h"

class USkeletalMeshComponent;
class UArenaWeaponStats;

UCLASS()
class SHOOTER_API AArenaWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	AArenaWeapon();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character, meta = (AllowPrivateAccess = "true"))
	UArenaWeaponStats* StatsComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Skeletal Mesh")
	USkeletalMeshComponent* MeshComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Arrow")
	UArrowComponent* Muzzle;
	UPROPERTY(EditAnywhere, Category = "Bullet")
	TSubclassOf<AArenaBullet> Bullet;

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void CalculateBulletSpawnData(FVector& SpawnPosition, FRotator& SpawnRotation);

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void Fire();
	virtual float GetFireRate();
};
