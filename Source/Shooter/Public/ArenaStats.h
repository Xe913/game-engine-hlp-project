#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ArenaStats.generated.h"

UENUM()
enum class StatUpdateType { Override, Add, Multiply };

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER_API UArenaStats : public UActorComponent
{
	GENERATED_BODY()

public:
	UArenaStats();

	void UpdateStat(const FString& StatName, float StatValue, StatUpdateType UpdateType = StatUpdateType::Override);

	//TODO template this
	float GetFloatStat(const FString& StatName, float DefaultValue = 0);

protected:
	//TODO template this
	void FindFloatProperty(void*& OutObject, FFloatProperty*& OutProperty, const FString& PropertyPath);
	virtual float ExecutePreUpdate(float NewValue, const FString& StatName);
	virtual float ExecutePostUpdate(float NewValue, const FString& StatName);

	UPROPERTY(EditAnywhere, Category = "Stats|Meta")
	FString MinPrefix = "Min";
	UPROPERTY(EditAnywhere, Category = "Stats|Meta")
	FString MaxPrefix = "Max";
};
