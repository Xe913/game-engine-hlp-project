#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ArenaHealthbarWidget.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_API UArenaHealthbarWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CurrentHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HealthPercentage;

public:
	void SetMaxHealth(float NewMaxHealth, bool UpdatePercentage = true);
	void SetCurrentHealth(float NewCurrentHealth, bool UpdatePercentage = true);
	void UpdateHealthPercentage();
};
