#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/WidgetComponent.h"
#include "ArenaCharacter.generated.h"

class AArenaWeapon;
class UArenaCharacterStats;
class UArenaCharacterInventory;
class UArenaHealthbarWidget;

UCLASS()
class SHOOTER_API AArenaCharacter : public ACharacter
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_DELEGATE_OneParam(FOnKill, AArenaCharacter*, KilledCharacter);

public:
	AArenaCharacter();

protected:
	UPROPERTY(EditDefaultsOnly, Category = Character)
	TSubclassOf<AArenaWeapon> Weapon;
	UPROPERTY(EditDefaultsOnly, Category = Character)
	FName WeaponSocket = "GunSocket";
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character, meta = (AllowPrivateAccess = "true"))
	UArenaCharacterStats* StatsComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character, meta = (AllowPrivateAccess = "true"))
	UArenaCharacterInventory* InventoryComponent;
	UPROPERTY(EditDefaultsOnly, Category = Character, meta = (AllowPrivateAccess = "true"))
	UWidgetComponent* HealthBarWidgetComponent;
	UPROPERTY(EditDefaultsOnly, Category = Character, meta = (AllowPrivateAccess = "true"))
	bool ContinuousFire = false;
	UPROPERTY(EditDefaultsOnly, Category = Character, meta = (AllowPrivateAccess = "true"))
	bool DisplayHealthbar = true;

	UPROPERTY(EditDefaultsOnly, Category = "Character|Damage Calculation", meta = (AllowPrivateAccess = "true"))
	TArray<FString> AddToBaseDamageStats;
	UPROPERTY(EditDefaultsOnly, Category = "Character|Damage Calculation", meta = (AllowPrivateAccess = "true"))
	TArray<FString> MultiplierStats;
	UPROPERTY(EditDefaultsOnly, Category = "Character|Damage Calculation", meta = (AllowPrivateAccess = "true"))
	TArray<FString> AddToMultipliedDamageStats;

	AArenaWeapon* EquippedWeapon;

	bool Firing;

	void MoveUp(float Value);
	void MoveRight(float Value);
	void Fire();
	virtual float CalculateFireInterval();
	virtual void DestroyCharacter();


public:
	virtual void BeginPlay() override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	virtual void Tick(float DeltaTime) override;

	void StartFiring();
	void StopFiring();

	virtual float GetTotalDamage(float BaseDamage);
	virtual void ProcessDamage(float Damage);

	void UpdateFireRate(float NewFireRate);
	virtual void UpdateHealthbar();

	void SetHealthbarVisibility(bool MakeVisible);

	bool IsFiring();

	FOnKill OnKill;

	FTimerHandle KeepFiringHandle;

};
