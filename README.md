**GAME ENGINE + HIGH LEVEL PROGRAMMING END OF TERM PROJECT**

**PERSONAGGI IN SCENA**

**ArenaCharacter** - classe base, gestisce le funzioni in comune e ha un'implementazione basilare della UI, healthbar come esempio. I componenti Stats e Inventory devono essere inizializzati nei figli con la versione corretta della classe.
Il character può essere impostato per avere sparo manuale o continuo. Se manuale, ogni chiamata a StartFiring (che avvenga per pressione di un tasto o comando del controller della IA) chiama Fire una sola volta. Se continuo, fa partire un delegate che chiama Fire ogni 1/(EquippedWeapon.FireRate * Character.FireRateMultiplier) secondi finché non viene chiamato StopFiring. Quando il FireRateMultiplier cambia il componente ArenaCharacterStats chiede a Character di fermare il delegate e farlo ripartire col nuovo intervallo come timer.

**ArenaPlayer** - il player ha un sistema di input proprio e resetta il livello quando i suoi punti vita arrivano a zero. Ha fuoco continuo, StartFiring e StopFiring vengono chiamati alla pressione e al rilascio del tasto sinistro del mouse.

**ArenaCharacterAim** - componente di ArenaPlayer che gestisce la mira col movimento del mouse.

**ArenaEnemy** - il nemico è gestito da ArenaIAController e non ha un Inventory, quindi non può raccogliere ArenaItem, e quando i suoi punti vita arrivano a zero lascia sul posto un ArenaItem casuale preso da un vettore di possibili drop, a seconda della percentuale di spawn. Ha fuoco continuo, StartFiring viene chiamato quando il player entra nel suo range di attacco, StopFiring quando esce.

**BARRA DELLA VITA**

**ArenaHealthbarWidget** - un semplice widget che mostra CurrentHealth (barra rossa) su MaxHealth (sfondo bianco). L'update è eseguito da ArenaCharacterStats quando una delle statistiche CurrentHealth o TotalHealth viene modificata. Si può chiedere esplicitamente di non aggiornare la percentuale di vita mostrata quando si aggiorna una delle due statistiche, in modo da non aggiornarla due volte se dovessero essere modificate entrambe contemporaneamente.

**ARMI E PROIETTILI**

**ArenaWeapon** - spara proiettili col metodo Fire, usa il metodo CalculateBulletSpawnData per decidere dove viene spawnato il proiettile e chiama SetupBulletData dello stesso allo spawn per definire le sue caratteristiche quali danno e velocità. In questo modo logica e statistiche vengono gestite in una sola classe e non c'è bisogno di modificare i BP dei proiettili se non per cambiare mesh o implementare comportamenti molto particolari (vedi ArenaBullet).
In questo gioco le armi hanno statistiche non modificabili come BaseDamage, Initial/Max Speed, Range e FireRate, contenute in ArenaWeaponStats, e il comportamento dei proiettili viene definito combinandole con quelle del Character, come DamageMultiplier e FireRateMultiplier, che sono invece modificabili raccogliendo oggetti. Niente vieta che in futuro le armi dispongano di un sistema di modifica simile ad ArenaCharacterInventory per modificare le proprie statistiche.

**ArenaBullet** - proiettile sparato. In SetupBulletData l'arma gli fornisce tutte le informazioni per funzionare correttamente. Si possono creare proiettili speciali modificando SetupBulletData ma finché il proiettile deve "solo andare dritto" si può usare quello base su qualsiasi arma, purché il BP abbia collision set ArenaPlayerBullet/ArenaEnemyBullet a seconda di chi lo usa.

**SPAWN DEI NEMICI**
**ArenaGameManager** - contiene le classi "manager", al momento solo lo SpawnerManager.

ArenaSpawnerManager - gestisce la logica di Spawn, spawnando un nuovo nemico ogni tot secondi in modo coerente, ovvero da uno spawner che non ha già spawnato il suo massimo numero di nemici e solo se il numero di nemici in scena è minore del massimo. Può essere facilmente personalizzato per modificare questa logica. Alcuni degli attributi e getter, come EnemiesLeft, non sono utilizzati nella corrente versione del gioco.

**ArenaSpawner** - si registra allo SpawnerManager e viene chiamato da esso per spawnare nemici. Si possono settare valori quali numero massimo di nemici spawnati o quantità di nemici da spawnare in totale prima di disattivarsi. E' possibile richiedere uno spawn forzato che ignora queste condizioni in caso si voglia usare lo spawner per eseguire eventi particolari. All'inizio doveva avere una barra degli hp e muoversi, e veniva richiesto al player di distruggerlo per evitare lo spawn di nuovi nemici, ma l'idea è stata abbandonata.

**IA**

**ArenaAIController** - per questa versione la IA è estremamente basilare. I nemici inseguono il player, iniziano a sparare quando è in range e smettono di sparare quando non c'è più.

**REFLECTION - INVENTARIO**

**ArenaCharacterInventory** - aggiunge gli item al player. Si tratta al momento solo di potenziamenti statistici aggiunti al valore corrente, quindi la classe si limita ad eseguire un UpdateStat con StatUpdateType::Add. La possibilità di raccogliere un ArenaItem dipende dalla presenza di un Inventory istanziato sul Character, quindi basterebbe istanziarne uno sui nemici se si volesse permettere loro di potenziarsi raccogliendo oggetti da terra (pessima idea, probabilmente). Se in una versione futura le armi dovessero essere personalizzabili, dovrebbe essere sufficiente inserire una versione dell'Inventory anche in esse, oppure un'altra classe che chiama l'update delle statistiche in modo simile.

**REFLECTION - STATISTICHE**

**ArenaStats** - classe base coi metodi di gestione delle statistiche. Scegliendo un StatUpdateType si può sovrascrivere una statistica (Override, di default), sommarvi un valore (Add) o moltiplicarla per esso (Multiply). Ogni statistica può inoltre essere associata ad altre due con lo stesso nome e i prefissi Min e Max (personalizzabili anche BP nella sezione "Stats|Meta"). Se queste due statistiche sono presenti, a ogni richiesta di update il valore da sostituire al corrente, dopo l'eventuale somma/moltiplicazione, se richiesta, viene clampato tra essi.<br>
Idealmente la classe va estesa creando classi figlie con statistiche proprie, senza toccare la logica che le modifica. Sono tuttavia presenti i metodi virtuali ExecutePreUpdate e ExecutePostUpdate dov'è possibile implementare comportamenti specifici quando una certa statistica viene modificata. PreUpdate viene eseguito per primo e il valore trovato tramite reflection viene aggiornato col suo valore di ritorno, poi viene eseguito l'update vero e proprio della statistica, e poi PostUpdate. PostUpdate ritorna a sua volta il valore modificato per simmetria, ma questo non viene mai letto nella logica base di update.<br>
Il clamp della statistica viene eseguito sia prima che dopo PreUpdate, quindi quando si implementa un particolare sistema di statistiche si possono scrivere Pre e Post con la sicurezza che i dati in ingresso sono coerenti e anche creando un'incoerenza in uscita questa non può compromettere il sistema.

**ArenaCharacterStats** - statistiche dei character. In PostUpdate si chiede al Character di aggiornare l'intervallo di sparo automatico se FireRateMultiplier è stato aggiornato, dato che il valore va a influenzare il timer di un delegate invisibile all'esterno della classe stessa, e in caso di modifiche a CurrentHealth o TotalHealth il valore viene sempre clampato in modo che CurrentHealth <= TotalHealth.

**ArenaEnemyStats** - i nemici usano questa classe figlia di ArenaCharacterStats che impiega la stessa logica ma ha una statistica in più, il DropRate.

**ArenaWeaponStats** - in questo gioco le statistiche delle armi non possono essere modificate. La classe quindi è una semplice implementazione di ArenaStats con statistiche adeguate.

**ArenaItem** - oggetto che in overlap col player interagisce col suo inventario per modificare delle statistiche. Si possono modificare più statistiche con un solo item, ma nella versione corrente è necessario che il tipo di update (Override/Add/Multiply) sia uguale per tutte le statistiche di tutti gli item. Nel gioco sono presenti ArenaItem01, rosso, che aumenta i danni inflitti, ArenaItem02, blu, che aumenta la cadenza di fuoco, e ArenaItem03, verde, che ripristina i punti vita.

**GAME LOOP**

Il gioco finisce quando i punti vita del player vengono azzerati. Non c'è una schermata di fine partita, il livello semplicemente si resetta. Un sistema che gestisca questa funziona può essere facilmente implementato e usando l'evento che i nemici lanciano quando vengono distrutti si possono anche aggiungere un contatore di combo o un modo per vincere la partita dopo tot punti/uccisioni. Al momento la partita non termina finché non si perde.
